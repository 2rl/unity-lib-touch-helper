# Touch helper
Destination: https://jvirkovskiy@bitbucket.org/2rl/unity-lib-touch-helper.git  
Dependency: "2reallife.helpers.touch-helper": "https://bitbucket.org/2rl/unity-lib-touch-helper.git?path=/Assets/Scripts/Helpers/TouchHelper#1.0.0"

See https://2rll.atlassian.net/wiki/spaces/MC/pages/855274/TouchHelper for details.
